#!/usr/bin/env bash
set -e

source setup.conf
scripts_folder="./developer-tools/"
scripts=($(ls developer-tools | sed 's/\.[^.]*$//'))
prereqs_list=(unzip git curl wget)


install_prereqs(){
    # Detect OS
    os=$(uname)
    if [[ $(command -v $1) != "" ]]; then
        case $os in 
            Darwin)
            if [[ $(command -v brew) != "" ]]; then
                echo "Installing with $1"
                brew install $1
            fi
            ;;
            Linux)
            if [[ $(command -v dnf) != "" ]]; then
                echo "Installing with dnf"
                sudo dnf -y install $1
            elif [[ $(command -v apt) != "" ]]; then
                echo "Installing with apt"
                sudo apt-get update
                sudo apt-get install -y $1
            fi
            ;;
            *)
            echo "Installing the failed"
            exit 1
        esac
    else
        echo "$1 is already installed.."
    fi
}

for item in "${prereqs_list[@]}"
do
    echo "Installing $item"
    install_prereqs $item
done


for script in "${scripts[@]}";do
    if [[ ${!script} == "true" ]]; then
        echo "Executing $script.sh"     
        [[ -f ./developer-tools/"$script".sh ]] && /bin/bash -c $scripts_folder"$script".sh
    fi
done