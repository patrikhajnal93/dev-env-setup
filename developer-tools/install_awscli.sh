#!/usr/bin/env bash

# Detect OS
os=$(uname)

# Detect architecture
arch=$(uname -m) 

case $os in
    Darwin)
    curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
    sudo installer -pkg AWSCLIV2.pkg -target /
    aws --version
    ;;
    Linux)
    case $arch in
        x86_64)
        echo "Installing awscli for x86_64 Linux"
        curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
        unzip awscliv2.zip
        sudo ./aws/install
        ;;
        aarch)
        echo "Installing awscli for aarch Linux"
        curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip"
        unzip awscliv2.zip
        sudo ./aws/install
        ;;
        *)
        echo "Archicteture not supported"
        exit 1
    esac
    ;;
    *)
    echo "OS not supported"
    exit 1
esac