#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in 
    Darwin|Linux)
    echo "Installing brew../"
    NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    ;;
    *)
    echo "OS not supported"
    exit 1
esac