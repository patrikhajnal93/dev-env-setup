#!/usr/bin/env bash

# Detect OS
os=$(uname)
if [[ $(command -v unzip) != "" ]]; then
    case $os in 
        Darwin)
        if [[ $(command -v brew) != "" ]]; then
            echo "Installing with unzip"
            brew install unzip
        fi
        ;;
        Linux)
        if [[ $(command -v dnf) != "" ]]; then
            echo "Installing with dnf"
            sudo dnf -y install unzip
        elif [[ $(command -v apt) != "" ]]; then
            echo "Installing with apt"
            sudo apt-get update
            sudo apt-get install -y unzip
        fi
        ;;
        *)
        echo "Installing the failed"
        exit 1
    esac
fi