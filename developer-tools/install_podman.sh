#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in 
    Darwin)
    if [[ $(command -v brew) != "" ]]; then
        echo "Installing podman with brew"
        brew install podman
        echo """
        Execute:
        podman machine init
        podman machine start
        """
    else
        echo "Brew not installed. Exiting..."
        exit 1
    fi
    ;;
    *)
    echo "OS not supported"
    exit 1
esac
        