#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in 
    Darwin)
    if [[ $(command -v brew) != "" ]]; then
        echo "Installing git with brew"
        brew install --cask visual-studio-code
    else
        curl "https://code.visualstudio.com/sha/download?build=stable&os=darwin-universal" -o vs-code.app
        sudo mv -R vs-code.app /Applications/
    fi
    ;;
    Linux)
    curl -o code.deb -L https://go.microsoft.com/fwlink/?LinkID=760868
    apt install -y ./code.deb
    ;;
    *)
    echo "OS not supported"
    exit 1
esac