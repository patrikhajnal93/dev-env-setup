#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in 
    Darwin)
    curl -OL "https://go.dev/dl/go1.20.5.darwin-arm64.pkg"
    sudo installer -pkg go1.20.5.darwin-arm64.pkg -target /
    go version
    ;;
    Linux)
    curl -OL https://go.dev/dl/go1.20.5.linux-amd64.tar.gz
    rm -rf /usr/local/go && tar -C /usr/local -xzf go1.20.5.linux-amd64.tar.gz
    ;;
    *)
    echo "OS not supported"
    exit 1
esac