#!/usr/bin/env bash

# Detect OS
os=$(uname)
echo "OS detected: ${os}"
case $os in
  Darwin)
  if [[ $(command -v brew) != "" ]]; then
    echo "Installing kubectl with brew..."
    brew install kubectl
  else
    echo "Installing kubectl binary..."
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/arm64/kubectl"
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    kubectl version --client
  fi
  ;;
  Linux)
  echo "Installing binary for Linux x86_64"
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  kubectl version --client
  ;;
  *)
  echo "Current OS (${os}) is not supported"
  exit 1
esac