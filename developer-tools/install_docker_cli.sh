#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in 
    Darwin)
    curl https://download.docker.com/mac/static/stable/aarch64/docker-24.0.2.tgz -o docker-cli.tgz
    tar xzf docker-cli.tgz
    sudo mv docker/* /usr/local/bin/
    docker version
    ;;
    Linux)
    sudo apt-get update -y
    sudo apt-get install -y ca-certificates curl gnupg
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    echo \
    "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update -y
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    ;;
    *)
    echo "OS not suppored"
    exit 1
esac