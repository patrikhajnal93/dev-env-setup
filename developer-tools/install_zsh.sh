#!/usr/bin/env bash

# Detect OS
os=$(uname)
if [[ $(command -v unzip) != "" ]]; then
    case $os in 
        Darwin|Linux)
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        ;;
        *)
        echo "OS not supported"
        exit 1
    esac
fi

