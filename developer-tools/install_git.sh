#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in
    Darwin)
    if [[ $(command -v brew) != "" ]]; then
        echo "Installing git with brew"
        brew install git
    else
        echo "Brew is now installed. Please install XCode manually."
        exit 1
    fi
    ;;
    Linux)
    if [[ $(command -v dnf) != "" ]]; then
        echo "Installing with dnf"
        sudo dnf install -y git
    elif [[ $(command -v yum) != "" ]]; then
        echo "Installing with yum"
        sudo yum install -y git
    elif [[ $(command -v apt) != "" ]]; then
        echo "Installing with apt"
        sudo apt-get install -y git
    fi
    ;;
    *)
    echo "OS not supported"
    exit 1
esac