#!/usr/bin/env bash

# Detect OS
os=$(uname)
case $os in
    Darwin)
    echo "Installing gcloud cli for Apple Silicon"
    curl https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-436.0.0-darwin-arm.tar.gz -o gcloud.tar.gz
    tar -xvf gcloud.tar.gz
    ./google-cloud-sdk/install.sh
    rm -rf gcloud.tar.gz
    gcloud version
    ;;
esac
